
@extends('layouts.app')
@section('content')



<h1>Create New Book</h1>
<form method = 'post' action="{{action('BookController@store')}}">
{{csrf_field()}}
<div class = "form-group">
    <label for = "title">Book Name</label>
    <input type= "text" class = "form-control" name= "title">
    <br>
    <label for = "author">Author Name</label>
    <input type= "text" class = "form-control" name= "author">
    <br>
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add Book">
</div>

</form>

@endsection