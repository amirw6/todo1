@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>

<h1>This is your book list</h1>
<table>
  <tr>
    
    <th>Title</th>
    <th>Author</th>
  </tr>

    @foreach($books as $book)
    <tr>

      <!-- <td>{{$book->title}}</td> -->
      <!-- <td>{{$book->author}}</td> -->
      <td> 
      @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
      <a href= "{{route('books.edit', $book->id )}}"> {{$book->title}} </td>
        
      <td> <a href= "{{route('books.edit', $book->id )}}"> {{$book->author}}  </td>
      
    </tr>
    @endforeach

</table>
<a href="{{route('books.create')}}">Create New Book </a>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url:" {{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function( errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  

</html>
@endsection