<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('books')->insert(
            [
                [
                        
                        'title' => 'Harry Poter',
                        'author' => 'JK Rolling',
                        'user_id' => 1,
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        
                        'title' => 'Game Of Thrones',
                        'author' => 'Jeorge RR Martin',
                        'user_id' => 2,
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        
                        'title' => 'Chipopo in Africa',
                        'author' => 'Omer Adam',
                        'user_id' => 1,
                        'created_at' => date('Y-m-d G:i:s'),

                ],
                [
                        
                        'title' => 'Jinji',
                        'author' => 'Galila ron feder',
                        'user_id' => 1,
                        'created_at' => date('Y-m-d G:i:s'),

                ],
                [
                       
                       'title' => 'Yeladim Zigzag',
                       'author' => 'David Grossman',
                       'user_id' => 2,
                       'created_at' => date('Y-m-d G:i:s'),
                ]
                
            
                    ]);
            
    }
}