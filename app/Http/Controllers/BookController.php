<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Response;



class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $id=Auth::id();//or later change to 2
        $books = User::find($id)->books;

        //$books = Book::all();
        return view('books.index', ['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new Book();
        $id=Auth::id();
        $book -> title = $request-> title;
        $book -> author = $request-> author;
        $book -> user_id = $id;
        $book -> status = 0;
        $book -> save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        return view('books.edit',['book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        $book = Book::find($id);
        if(!$book->user->id == Auth::id()) return(redirect('books'));
        $book->update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'succes','status'=>$request->status),200);
        }
        return redirect('books');

        
        //$book ->update($request->all());
        //return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $book = Book::find($id);
        if(!$book->user->id == Auth::id()) return(redirect('books'));
        $book ->delete();
        return redirect('books');
    }
}
